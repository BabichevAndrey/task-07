package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final String URL = "jdbc:mysql://localhost:3306/test2db";
	private static final String USER = "root";
	private static final String PASSWORD = "root";

//	private static final String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";

	private static DBManager instance;

	public static synchronized DBManager getInstance() {
		if (instance == null)
			instance = new DBManager();
		return instance;
	}

	private DBManager() {
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
// transaction

//		Connection con = null;
//		PreparedStatement stmt = null;
//		try {
//			con = DriverManager.getConnection(URL, USER, PASSWORD);
//		//	con.getAutoCommit(false);
//			stmt = con.prepareStatement("INSERT INTO users (login) VALUES (?);", Statement.RETURN_GENERATED_KEYS);
//			stmt.setString(1, user.getLogin());
//
//
//			int count =  stmt.executeUpdate();
//			if (count > 0) {
//				try (ResultSet rs = stmt.getGeneratedKeys();){
//					if (rs.next()){
//						user.setId(rs.getInt(1));
//					}
//				}
//			}
//
//			for (Team team: teams) {
//				stmt = con.prepareStatement("INSERT INTO teams (name) VALUES (?);", Statement.RETURN_GENERATED_KEYS);
//				stmt.setString(1, team.getName());
//				int countTeam =  stmt.executeUpdate();
//				if (countTeam > 0) {
//					try (ResultSet rs = stmt.getGeneratedKeys();){
//						if (rs.next()){
//							user.setId(rs.getInt(1));
//						}
//					}
//				}
//			}
//
//			//	stmt.executeQuery();
//			con.commit();
//		} catch (SQLException e){
//			e.printStackTrace();
//			try {
//				con.rollback();
//			} catch (SQLException ex) {
//				ex.printStackTrace();
//			}
//			throw new DBException("setTeamsForUser error", e);
//		} finally {
//			if (stmt != null) {
//				try {
//					stmt.close();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
//			}
//			if (con != null) {
//				try {
//					con.close();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
//			}
//		}



		return false;
	}



	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();

		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			stmt = con.prepareStatement(" select * from teams as t join users_teams as ut on (t.id = ut.user_id) where ut.user_id = (select id from users where login = ? );");
			stmt.setString(1, user.getLogin());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Team team = new Team();
				team.setName(rs.getString("name"));
				team.setId(rs.getInt("id"));
				System.out.println(team.toString()+"77777777777");
				teams.add(team);
			}
		}catch (SQLException e){
			e.printStackTrace();
			throw new DBException("getUserTeams Exeption", e);
		}

		return teams;
	}








	public boolean deleteUsers(User... users) throws DBException {

		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			for (User user: users) {
				stmt = con.prepareStatement("DELETE FROM users where login = ?;", Statement.RETURN_GENERATED_KEYS);
				stmt.setString(1, user.getLogin());
				stmt.execute();
			}

		} catch (SQLException e){
			e.printStackTrace();
			throw new DBException("setTeamsForUser error", e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return true;
	}

	public boolean deleteTeam(Team team) throws DBException {

		Connection con = null;
		PreparedStatement stmt = null;
		PreparedStatement stmt1 = null;
		try {
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			stmt = con.prepareStatement("DELETE FROM teams where name = ?;", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, team.getName());
			stmt.execute();

			stmt1 = con.prepareStatement("DELETE FROM users_teams where team_id = ?;", Statement.RETURN_GENERATED_KEYS);
			stmt1.setInt(1, team.getId());
			stmt1.execute();
		} catch (SQLException e){
			e.printStackTrace();
			throw new DBException("setTeamsForUser error", e);
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return true;
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
			 Statement stmt = con.createStatement();
			 ResultSet rs = stmt.executeQuery("SELECT * FROM users;")){
			while (rs.next()){
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
				users.add(user);
			}
		} catch (SQLException e){
			e.printStackTrace();
			throw new DBException("FindAllUsers Exeption", e);
		}
		return users;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (Connection con = DriverManager.getConnection(URL, USER, PASSWORD);
			 Statement stmt = con.createStatement();
			 ResultSet rs = stmt.executeQuery("SELECT * FROM teams;")){
			while (rs.next()){
				Team team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
				teams.add(team);
			}
		} catch (SQLException e){
			e.printStackTrace();
			throw new DBException("FindAllTeams Exeption", e);
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {

		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			stmt = con.prepareStatement("INSERT INTO teams (name) VALUES (?)");
			stmt.setString(1, team.getName());
			stmt.execute();
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	public boolean insertUser(User user) throws DBException {

		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			stmt = con.prepareStatement("INSERT INTO users (login) VALUES (?);", Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, user.getLogin());
		//	int count =  stmt.executeUpdate();
//			if (count > 0) {
//				try (ResultSet rs = stmt.getResultSet()){
//					if (rs.next()){
//						user.setId(rs.getInt(1));
//					}
//
//				}
//			}

			stmt.execute();
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return true;
	}

	public User getUser(String login) throws DBException {

		User user = new User();
//		Connection con = null;
//		PreparedStatement stmt = null;
//		try {
//			con = DriverManager.getConnection(URL, USER, PASSWORD);
//			stmt = con.prepareStatement("SELECT * FROM users WHERE login = ?;");
//			stmt.setString(1, login);
//			ResultSet rs = stmt.executeQuery();
//			while (rs.next()) {
//				user.setLogin(rs.getString("name"));
//				user.setId(rs.getInt("id"));
//				break;
//			}
//		} catch (SQLException e){
//			e.printStackTrace();
//		} finally {
//			if (stmt != null) {
//				try {
//					stmt.close();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
//			}
//			if (con != null) {
//				try {
//					con.close();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
//			}
//		}

		return user;
	}

	public Team getTeam(String name) throws DBException {


		Team team = new Team();

		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			stmt = con.prepareStatement("SELECT * FROM teams WHERE name = ?;");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				team.setName(rs.getString("name"));
				team.setId(rs.getInt("id"));
				break;
			}
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return team;
	}

	public boolean updateTeam(Team team) throws DBException {

		Connection con = null;
		PreparedStatement stmt = null;
		try {
			con = DriverManager.getConnection(URL, USER, PASSWORD);
			stmt = con.prepareStatement("UPDATE teams SET name = ? WHERE id = ?;");
			stmt.setString(1, team.getName());
			stmt.setInt(2, team.getId());
			stmt.execute();
		} catch (SQLException e){
			e.printStackTrace();
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return true;
	}


}
