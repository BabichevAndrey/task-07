package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public User() {
	}

	public User(String login) {
		super();
		this.login = login;
	}

	// no exist
	@Override
	public String toString() {
		return login;
	}
// no exist
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		User user = (User) o;
		return Objects.equals(login, user.login);
	}

	@Override
	public int hashCode() {
		return Objects.hash(login);
	}

	public void setLogin(String login) {
		this.login = login;
	}

	// was return null
	public static User createUser(String login) {
		User user = new User();
		user.setLogin(login);
		return user;
	}

}
